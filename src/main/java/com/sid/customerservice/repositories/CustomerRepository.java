package com.sid.customerservice.repositories;

import com.sid.customerservice.domain.Customer;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, UUID> {

    Page<Customer> findAll(final Pageable pageable);
    Page<Customer> findAllByEmail(final String email, final Pageable pageable);
    Page<Customer> findAllByNameAndEmail(final String name, final String email, final Pageable pageable);

    Optional<Customer> findById(final UUID customerId);
}
