package com.sid.customerservice.web.utils;

public interface Constants {

    final static String APP_ROOT = "api/v1/customer";

    final static String CUSTOMER_ENDPOINT = APP_ROOT ;
    final static String CREATE_CUSTOMER_ENDPOINT = CUSTOMER_ENDPOINT + "/create";
    final static String FIND_CUSTOMER_BY_ID_ENDPOINT = CUSTOMER_ENDPOINT + "/{customerId}";
    final static String FIND_ALL_CUSTOMERS_ENDPOINT = CUSTOMER_ENDPOINT + "/all";
}
