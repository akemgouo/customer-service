package com.sid.customerservice.web.controller.api;

import com.sid.customerservice.web.dto.CustomerDto;
import com.sid.customerservice.web.dto.CustomerPagedList;
import io.swagger.annotations.Api;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

import static com.sid.customerservice.web.utils.Constants.*;
import static com.sid.customerservice.web.utils.Constants.FIND_ALL_CUSTOMERS_ENDPOINT;

@Tag(name = "Customer Api", description = "the Customer API with documentation annotations")
@Api(value = APP_ROOT , protocols = "http, https", produces =  MediaType.APPLICATION_JSON_VALUE, tags = "Customer Api")
public interface CustomerApi {
    @Operation(summary = "Save customer infos", description = "Save the customer. The operation returns the current customer saved")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Created Customer"),
            @ApiResponse(responseCode = "400", description = "Bad request"),
            @ApiResponse(responseCode = "500", description = "Server Error")})
    @PostMapping(value = CREATE_CUSTOMER_ENDPOINT, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<CustomerDto> save(@RequestBody @Validated final CustomerDto customerDto);

    @Operation(summary = "Get user details", description = "Get the customer details. The operation returns the details of the customer.")
    @GetMapping(value = FIND_CUSTOMER_BY_ID_ENDPOINT, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Le customer à été trouver"),
            @ApiResponse(responseCode = "204", description = "Le customer n'a pas été trouver dans la BD")
    })
    ResponseEntity<CustomerDto> findCustomerById(@PathVariable("customerId") final UUID customerId);

    /*@Operation(summary = "Get all the customer details", description = "Get the list of customer. The operation returns the list of the customer")
    @GetMapping(value = FIND_ALL_CUSTOMERS_ENDPOINT, produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<CustomerPagedList> listCustomers(@RequestParam(value = "pageNumber", required = false) final Integer pageNumber,
                                                    @RequestParam(value = "pageSize", required = false) final Integer pageSize,
                                                    @RequestParam(value = "name", required = false) final String name,
                                                    @RequestParam(value = "email", required = false) final String email);*/

    @Operation(summary = "Get all the customer details", description = "Get the list of customer. The operation returns the list of the customer")
    @GetMapping(value = FIND_ALL_CUSTOMERS_ENDPOINT, produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<List<CustomerDto>> findAll();
}
