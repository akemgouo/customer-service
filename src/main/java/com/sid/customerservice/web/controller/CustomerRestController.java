package com.sid.customerservice.web.controller;

import com.sid.customerservice.services.CustomerService;
import com.sid.customerservice.web.controller.api.CustomerApi;
import com.sid.customerservice.web.dto.CustomerDto;
import com.sid.customerservice.web.dto.CustomerPagedList;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.UUID;

@RestController
@RequiredArgsConstructor
@RefreshScope
public class CustomerRestController implements CustomerApi {

    @Value("${DEFAULT_PAGE_NUMBER}")
    private String DEFAULT_PAGE_NUMBER;

    @Value("${DEFAULT_PAGE_SIZE}")
    private String DEFAULT_PAGE_SIZE;

    private final CustomerService customerService;

    @Override
    public ResponseEntity<CustomerDto> save(final CustomerDto customerDto) {
        return ResponseEntity.status(HttpStatus.CREATED).body(customerService.save(customerDto));
    }

    @Override
    public ResponseEntity<CustomerDto> findCustomerById(final UUID customerId) {
        return ResponseEntity.status(HttpStatus.OK).body(customerService.getCustomerById(customerId));
    }

    /*@Override
    public ResponseEntity<CustomerPagedList> listCustomers(Integer pageNumber, Integer pageSize, final String name, final String email) {

        System.out.println(DEFAULT_PAGE_NUMBER);
        System.out.println(DEFAULT_PAGE_SIZE);
        if (pageNumber == null || pageNumber < 0){
            pageNumber = Integer.valueOf(DEFAULT_PAGE_NUMBER);
        }

        if (pageSize == null || pageSize < 1) {
            pageSize = Integer.valueOf(DEFAULT_PAGE_SIZE);
        }

        CustomerPagedList customerPagedList = customerService.listCustomers(name, email, PageRequest.of(pageNumber,pageSize));

        return ResponseEntity.status(HttpStatus.OK).body(customerPagedList);
    }*/

    @Override
    public ResponseEntity<List<CustomerDto>> findAll() {
        return ResponseEntity.status(HttpStatus.OK).body(customerService.findAll());
    }
}
