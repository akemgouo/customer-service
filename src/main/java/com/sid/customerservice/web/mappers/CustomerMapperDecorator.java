package com.sid.customerservice.web.mappers;

import com.sid.customerservice.domain.Customer;
import com.sid.customerservice.web.dto.CustomerDto;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class CustomerMapperDecorator implements CustomerMapper {

    private CustomerMapper customerMapper;

    @Autowired
    public void setCustomerMapper(final CustomerMapper customerMapper){this.customerMapper = customerMapper;}

    @Override
    public CustomerDto convertEntityToDto(final Customer customer){
        return customerMapper.convertEntityToDto(customer);
    }

    @Override
    public Customer convertDtoToEntity(final CustomerDto customerDto){
        return customerMapper.convertDtoToEntity(customerDto);
    }
}
