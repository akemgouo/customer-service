package com.sid.customerservice.web.mappers;

import com.sid.customerservice.domain.Customer;
import com.sid.customerservice.web.dto.CustomerDto;
import org.mapstruct.DecoratedWith;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(uses = DateMapper.class)
@DecoratedWith(CustomerMapperDecorator.class)
public interface CustomerMapper {

    @Mapping(source = "customer.id", target = "customerId")
    @Mapping(source = "customer.lastModifiedDate", target = "lastModifiedDate"/*, nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS*/)
    @Mapping(source = "customer.createdDate", target = "createdDate"/*, nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS*/)
    CustomerDto convertEntityToDto(final Customer customer);

    @Mapping(source = "customerDto.customerId", target = "id")
    @Mapping(source = "customerDto.lastModifiedDate", target = "lastModifiedDate"/*, nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS*/)
    @Mapping(source = "customerDto.createdDate", target = "createdDate"/*, nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS*/)
    Customer convertDtoToEntity(final CustomerDto customerDto);

}
