package com.sid.customerservice;

import com.sid.customerservice.domain.Customer;
import com.sid.customerservice.repositories.CustomerRepository;
import com.sid.customerservice.services.CustomerService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Bean;

import java.util.List;

@SpringBootApplication
@EnableDiscoveryClient
public class CustomerServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(CustomerServiceApplication.class, args);
	}

	@Bean
	CommandLineRunner start(final CustomerRepository customerRepository){
		return args -> {
			customerRepository.saveAllAndFlush(List.of(
					Customer.builder().name("Arnaud").email("akemgouo@cfao.com").build(),
					Customer.builder().name("Hassan").email("hassan@cfao.com").build(),
					Customer.builder().name("IMane").email("imane@cfao.com").build(),
					Customer.builder().name("Iban").email("iban@cfao.com").build()
			));

			customerRepository.findAll().forEach(System.out::println);
		};
	}
}
