package com.sid.customerservice.services;

import com.sid.customerservice.web.dto.CustomerDto;
import com.sid.customerservice.web.dto.CustomerPagedList;
import org.hibernate.type.StringNVarcharType;
import org.springframework.data.domain.PageRequest;

import java.util.List;
import java.util.UUID;

public interface CustomerService {

    CustomerPagedList listCustomers(final String name, final String email, PageRequest pageRequest);

    List<CustomerDto> findAll();

    CustomerDto getCustomerById(final UUID customerId);

    CustomerDto save(final CustomerDto customerDto);

}
