package com.sid.customerservice.services;

import com.sid.customerservice.domain.Customer;
import com.sid.customerservice.repositories.CustomerRepository;
import com.sid.customerservice.web.dto.CustomerDto;
import com.sid.customerservice.web.dto.CustomerPagedList;
import com.sid.customerservice.web.mappers.CustomerMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor
public class CustomerServiceImpl implements CustomerService {

    private final CustomerRepository customerRepository;
    private final CustomerMapper customerMapper;

    @Override
    public CustomerPagedList listCustomers(final String name, final String email, final PageRequest pageRequest) {

        System.out.println("I was called");
        Page<Customer> customerPage;
        CustomerPagedList customerPagedList;
        if((name != null || StringUtils.hasLength(name))&& (email != null || StringUtils.hasLength(email))){
            customerPage = customerRepository.findAllByNameAndEmail(name,email,pageRequest);
        }else if(name == null && (email != null || StringUtils.hasLength(email))){
            customerPage = customerRepository.findAllByEmail(email, pageRequest);
        }else {
            customerPage = customerRepository.findAll(pageRequest);
        }
        customerPagedList = new CustomerPagedList(customerPage
                                                    .getContent()
                                                    .stream()
                                                    .map(customerMapper::convertEntityToDto)
                                                    .collect(Collectors.toList()), PageRequest.of(customerPage.getPageable().getPageNumber(),
                                                                                                    customerPage.getPageable().getPageSize()
                                                                                                ),
                                                                                    customerPage.getTotalElements()
                                                );


        return customerPagedList;
    }

    @Override
    public List<CustomerDto> findAll() {
        return customerRepository.findAll()
                .stream()
                .map(customerMapper::convertEntityToDto)
                .collect(Collectors.toList());
    }

    @Override
    public CustomerDto getCustomerById(final UUID customerId) {
        if(customerId == null){
            log.error("L'objet customerId est null");
            return null;
        }
        return customerRepository.findById(customerId)
                .map(customerMapper::convertEntityToDto)
                .orElseThrow(
                        () -> new EntityNotFoundException("Le customer fourni avec l'identifiant customerId= " + customerId + " est inexistant")
                );
    }

    @Override
    public CustomerDto save(final CustomerDto customerDto) {
        return customerMapper.convertEntityToDto(customerRepository.saveAndFlush(customerMapper.convertDtoToEntity(customerDto)));
    }
}
